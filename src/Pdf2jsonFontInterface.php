<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

use Stringable;

/**
 * Pdf2jsonFontInterface interface file.
 * 
 * This represents extracted font data from a given document.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface Pdf2jsonFontInterface extends Stringable
{
	
	/**
	 * Gets the id of this font spec.
	 * 
	 * @return int
	 */
	public function getFontspec() : int;
	
	/**
	 * Gets the size of the font.
	 * 
	 * @return int
	 */
	public function getSize() : int;
	
	/**
	 * Gets the family of the font.
	 * 
	 * @return string
	 */
	public function getFamily() : string;
	
	/**
	 * Gets the color of the font.
	 * @todo transform into html color code object
	 * 
	 * @return string
	 */
	public function getColor() : string;
	
}
