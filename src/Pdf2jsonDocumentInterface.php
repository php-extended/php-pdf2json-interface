<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

use Stringable;

/**
 * Pdf2jsonDocumentInterface interface file.
 * 
 * This represents extracted data from a given document.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface Pdf2jsonDocumentInterface extends Stringable
{
	
	/**
	 * Gets the number of this page.
	 * 
	 * @return int
	 */
	public function getNumber() : int;
	
	/**
	 * Gets the quantity of pages.
	 * 
	 * @return int
	 */
	public function getPages() : int;
	
	/**
	 * Gets the height of the page.
	 * 
	 * @return int
	 */
	public function getHeight() : int;
	
	/**
	 * Gets the width of the page.
	 * 
	 * @return int
	 */
	public function getWidth() : int;
	
	/**
	 * Gets the fonts found in the document.
	 * 
	 * @return array<int, Pdf2jsonFontInterface>
	 */
	public function getFonts() : array;
	
	/**
	 * Gets the text snippets found in the document.
	 * 
	 * @return array<int, Pdf2jsonTextInterface>
	 */
	public function getText() : array;
	
}
