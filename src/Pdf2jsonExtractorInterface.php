<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

use InvalidArgumentException;
use LogicException;
use RuntimeException;
use Stringable;

/**
 * Pdf2jsonExtractorInterface class file.
 * 
 * This represents an object that has the logic to extract pdf2json data from
 * a given pdf file or raw data.
 * 
 * @author Anastaszor
 */
interface Pdf2jsonExtractorInterface extends Stringable
{
	
	/**
	 * Extracts the data of the pdf at the given file path. If the `useLocalJson`
	 * is true, then if a json file exists where it should be generated, it
	 * wont be regenerated. If the `eraseLocalJson` is true, then the generated
	 * json will be removed after been parsed.
	 * 
	 * @param string $pdfFilePath
	 * @param boolean $useLocalJson
	 * @param boolean $eraseLocalJson
	 * @return Pdf2jsonDocumentInterface
	 * @throws RuntimeException if the data cannot be extracted
	 * @throws LogicException if the underlying extractor is not installed
	 * @throws InvalidArgumentException if the file does not exists at path
	 */
	public function extractFromPdfFile(string $pdfFilePath, bool $useLocalJson = true, bool $eraseLocalJson = true) : Pdf2jsonDocumentInterface;
	
	/**
	 * Extracts the data of the given pdf, using a temporary directory to
	 * write the pdf file and the json file.
	 * 
	 * @param string $pdfString
	 * @param ?string $tempDir
	 * @return Pdf2jsonDocumentInterface
	 * @throws RuntimeException if the data cannot be extracted
	 * @throws LogicException if the underlying extractor is not installed
	 * @throws InvalidArgumentException if the directory does not exists
	 */
	public function extractFromPdfString(string $pdfString, ?string $tempDir = null) : Pdf2jsonDocumentInterface;
	
}
