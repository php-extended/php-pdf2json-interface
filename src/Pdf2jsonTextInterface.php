<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

use Stringable;

/**
 * Pdf2jsonTextInterface interface file.
 * 
 * This represents exacted text data from a given document.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface Pdf2jsonTextInterface extends Stringable
{
	
	/**
	 * Gets the absolute position from top border of the page of this text.
	 * 
	 * @return int
	 */
	public function getTop() : int;
	
	/**
	 * Gets the absolute position from left border of the page of this text.
	 * 
	 * @return int
	 */
	public function getLeft() : int;
	
	/**
	 * Gets the width of this font snippet.
	 * 
	 * @return int
	 */
	public function getWidth() : int;
	
	/**
	 * Gets the height of this font snippet.
	 * 
	 * @return int
	 */
	public function getHeight() : int;
	
	/**
	 * Gets the font spec of this text snippet.
	 * 
	 * @return int
	 */
	public function getFont() : int;
	
	/**
	 * Gets the text data found.
	 * 
	 * @return string
	 */
	public function getData() : string;
	
}
